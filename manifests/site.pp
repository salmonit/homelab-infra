class { 'docker':
  version => 'latest',
}

class {'docker::compose':
  ensure => present,
  version => '1.25.0',
}

include docker_env
