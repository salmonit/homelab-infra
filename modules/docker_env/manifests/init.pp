class docker_env {

  include 'docker'

  $sabnzbd_tag = lookup('docker_env::sabnzbd_tag')
  $jellyfin_tag = lookup('docker_env::jellyfin_tag')
  $traefik_tag = lookup('docker_env::traefik_tag')
  $unifi_tag = lookup('docker_env::unifi_tag')
  $openhab_tag = lookup('docker_env::openhab_tag')
  $mosquitto_tag = lookup('docker_env::mosquitto_tag')
  $pihole_tag = lookup('docker_env::pihole_tag')
  $gitlab_runner_tag = lookup('docker_env::gitlab_runner_tag')
  $gitlab_runner_token = lookup('docker_env::gitlab_runner_token')
  $cf_api_token = lookup('docker_env::cf_api_token')
  
  file { '/opt':
    ensure => directory
  }

  file { '/opt/homelab_infra':
    ensure => directory
  }

  file { '/opt/homelab_infra/docker-compose.yml':
    ensure  => file,
    content => template('docker_env/docker-compose.erb')
  }

  file { '/opt/homelab_infra/traefik':
    ensure => directory
  }

  file { '/opt/homelab_infra/traefik/traefik.yml':
    ensure => file,
    content => file('docker_env/traefik.yml')
  }

  file { '/opt/homelab_infra/traefik/dynamic_conf.yml':
    ensure => file,
    content => file('docker_env/dynamic_conf.yml')
  }

  file {'/pool/docker_volumes/pihole_dnsmasq.d/02-kirstein.conf':
    ensure  => file,
    content => file('docker_env/02-kirstein.conf')
  }

  docker_compose { 'homelab':
    ensure        => present,
    compose_files => ['/opt/homelab_infra/docker-compose.yml'],
    subscribe     => [
      File['/opt/homelab_infra/docker-compose.yml'],
      File['/pool/docker_volumes/mosquitto/config/mosquitto.conf'],
      File['/pool/docker_volumes/pihole_dnsmasq.d/02-kirstein.conf'],
      File['/pool/docker_volumes/gitlab-runner/config/config.toml'],
      File['/opt/homelab_infra/traefik/traefik.yml'],
      File['/opt/homelab_infra/traefik/dynamic_conf.yml']]
  }

  file { '/pool/docker_volumes/mosquitto':
    ensure => directory
  }

  file { '/pool/docker_volumes/mosquitto/config':
    ensure => directory
  }

  file { '/pool/docker_volumes/mosquitto/config/mosquitto.conf':
    ensure  => file,
    content => file('docker_env/mosquitto.conf')
  }

  file { '/pool/docker_volumes/gitlab-runner':
    ensure => directory
  }

  file { '/pool/docker_volumes/gitlab-runner/config':
    ensure => directory
  }

  file { '/pool/docker_volumes/gitlab-runner/config/config.toml':
    ensure  => file,
    content => template('docker_env/config.erb')
  }
}
